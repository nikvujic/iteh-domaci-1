<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <title>Document</title>
</head>
<body>
    <div class="container">
        <div class="row info-row">
            <div class="col">
                <p class="title-p">Library</p>
            </div>
        </div>
        <div class="row content-row">
            <div class="col-sm-8 books-col">
                <div class="container-fluid books-side-container">
                    <div class="row controls-row">
                        <div class="col controls-col">
                            <div class="container-fluid">
                                <div class="row">
                                    <div id="refresh-btn" class="col-sm-3 filter">
                                        refresh
                                    </div>
                                    <div id="sort-btn" class="col-sm-3 filter">
                                        sort by name
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-sm-10">
                                                    <input id="book-name-input" class="search-input" type="text">
                                                </div>
                                                <div class="col-sm-2 search-img-col">
                                                    <img id="search-btn" class="search-icon" src="./img/magnifier.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row books-row">
                        <div id="booksPanel" class="container-fluid books-container">
                            <!-- <div class="row book-row">
                                <div class="col book-cover-col">
                                    <img class="book-cover" src="./img/jsfg.jpg" alt="">
                                    <p class="info-p">NAME <br> AUTHOR <br> 3/5</p>
                                </div>
                            </div>
                            <div class="row book-row">
                                <div class="col book-cover-col">
                                    <img class="book-cover" src="./img/pme.jpg" alt="">
                                    <p class="info-p">NAME <br> AUTHOR <br> 2/5</p>
                                </div>
                            </div>
                            <div class="row book-row">
                                <div class="col book-cover-col">
                                    <img class="book-cover" src="./img/bog.jpg" alt="">
                                    <p class="info-p">NAME <br> AUTHOR <br> 4/5</p>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="container-fluid right-side">
                    <div class="row">
                        <div class="col">
                            <p id="add-new-btn" class="add-new-p">ADD NEW</p>
                        </div>
                    </div>
                    <div class="row book-desc-row">
                        <div class="col">
                            <div class="container-fluid">
                                <div class="row info-img-row">
                                    <img id="selected-book-img" class="info-book-cover" src="./img/default.png" alt="">
                                </div>
                                <div class="row info-text-row">
                                    <p id="selected-book-rating" class="info-text-p">Rating</p>
                                </div>
                                <div class="row info-text-row">
                                    <p id="selected-book-isbn" class="info-text-p">ISBN</p>
                                </div>
                                <div class="row info-text-row">
                                    <p id="selected-book-name" class="info-text-p">Book name</p>
                                </div>
                                <div class="row info-text-row">
                                    <p id="selected-book-genre" class="info-text-p">Genre</p>
                                </div>
                                <div class="row info-text-row">
                                    <p id="selected-book-author" class="info-text-p">Author</p>
                                </div>
                                <div class="row info-text-row">
                                    <p id="selected-book-description" class="info-text-p">Description</p>
                                </div>
                                <div id="edit-btn" class="row info-edit-row">
                                    <p class="info-edit-p">EDIT</p>
                                </div>
                                <div id="delete-btn" class="row info-delete-row">
                                    <p class="info-delete-p">DELETE</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="new-book-modal" class="modal">
        <div class="modal-content">
            <form action="/action_page.php">
                <label>Name</label>
                <input id="new-book-name" class="book-input" type="text" placeholder="Book name..">

                <label>Author</label>
                <input id="new-book-author" class="book-input" type="text" placeholder="Author name..">

                <label>ISBN</label>
                <input id="new-book-isbn" class="book-input" type="text" placeholder="Book ISBN..">

                <label>Rating</label>
                <div class="rating">
                    <input id="rating1" class="rating" type="radio" name="new-rating" value="1">
                    <label for="rating1">1</label>
                    <input id="rating2" class="rating" type="radio" name="new-rating" value="2">
                    <label for="rating2">2</label>
                    <input id="rating3" class="rating" type="radio" name="new-rating" value="3">
                    <label for="rating3">3</label>
                    <input id="rating4" class="rating" type="radio" name="new-rating" value="4">
                    <label for="rating4">4</label>
                    <input id="rating5" class="rating" type="radio" name="new-rating" value="5">
                    <label for="rating5">5</label>
                </div> <br>
                
                <label>Genre</label>
                <select id="genreContainer" class="genre-select">
                
                </select>

                <label for="subject">Description</label>
                <textarea id="description-textarea" class="description-textarea" placeholder="Write something.." style="height:200px"></textarea>

                <input id="new-book-submit-btn" class="submit-button" type="button" value="ADD NEW BOOK">
                <input id="edit-book-submit-btn" class="submit-button" type="button" value="EDIT BOOK">
            </form>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
</body>
</html>