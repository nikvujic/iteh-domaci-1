<?php

class DbBroker {

	private $mysql_server = "localhost";
	private $mysql_user = "root";
	private $mysql_password = "";
	private $mysql_db = "iteh_db";
	private $mysqli;
	private static $instance = null;

	private function __construct(){
		$this->mysqli = new mysqli($this->mysql_server, $this->mysql_user, $this->mysql_password, $this->mysql_db);
		if ($this->mysqli->connect_errno) {
    		printf("Connection error: %s\n", $this->mysqli->connect_error);
    		exit();
		}
		$this->mysqli->set_charset("utf8");
	}

	public static function getInstance(){
		if (self::$instance == null){
			self::$instance = new DbBroker();
		}
		return self::$instance;
	}

	public function query($query){
		return $this->mysqli->query($query);
	}

}

?>