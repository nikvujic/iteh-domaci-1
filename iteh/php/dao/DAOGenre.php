<?php

	include_once "DAO.php";
	include "../model/Genre.php";

	class DAOGenre extends DAO {

		private $tableName = 'genre';

		function __construct() {
			parent::__construct();
		}

		public function populateObjectFromDBRow($dbRow) { 
			return new Genre($dbRow->name, $dbRow->genre_id);
		}

		public function getById($genreId) {
			$query = "SELECT * FROM " . $this->tableName . " WHERE genre_id = $genreId";
			return $this->getResult($query)[0];
		}

		public function getAll() {
			$query = "SELECT * FROM " . $this->tableName;
			return $this->getResult($query);
		}

		public function getAllSorted() {
		}

		public function insert($genre){
		}

		public function update($genre) {
		}

		public function delete($genreId) {
		}

	}

?>