<?php

	include_once "DbBroker.php";

	abstract class DAO {

		protected $dbBroker;

		public function __construct(){
			$this->dbBroker = DbBroker::getInstance();
		}

		public function getResult($query) {
			$result = $this->dbBroker->query($query);
			if (gettype($result) == 'object') {
				$arr = array();
				if ($result->num_rows > 0) {
					while ($dbRow = $result->fetch_object()) {
						$arr[] = $this->populateObjectFromDBRow($dbRow); 
					}
				}
				return $arr;
			}
			return null;
		}

		public abstract function populateObjectFromDBRow($dbRow);
		public abstract function getById($id);
		public abstract function getAll();
		public abstract function getAllSorted();
		public abstract function insert($object);
		public abstract function update($object);
		public abstract function delete($object);
		
	}

?>