<?php

class Book {

	public $bookId;
	public $isbn;
	public $name;
	public $description;
    public $rating;
    public $image;
	public $genreId;
	public $author;

	public function __construct($isbn, $name, $description, $rating, $image, $genreId, $author, $bookId = 0) {
		$this->bookId = $bookId;
		$this->isbn = $isbn;
		$this->name = $name;
		$this->description = $description;
        $this->rating = $rating;
        $this->image = $image;
		$this->genreId = $genreId;
		$this->author = $author;
	}

}

?>