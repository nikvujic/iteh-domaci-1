<?php

    include_once "../dao/DAOBook.php";

    $daoBook = new DAOBook();
    $method = $_SERVER['REQUEST_METHOD'];
    $response = 'OK';

    if ($method === 'GET') {
        if (isset($_GET['bookId'])) {
            $book = $daoBook->getById($_GET['bookId']);
            $response = json_encode($book);
        } else if (isset($_GET['sort'])) {
            $books = $daoBook->getAllSorted();
            $response = json_encode($books);
        } else if (isset($_GET['searchWord'])) {
            $books = $daoBook->search($_GET['searchWord']);
            $response = json_encode($books);
        } else {
            $books = $daoBook->getAll();
            $response = json_encode($books);
        }
    } else if ($method === 'POST') {
        $book = new Book($_POST['isbn'], $_POST['name'], $_POST['description'], $_POST['rating'], $_POST['image'], $_POST['genreId'], $_POST['author']);
        $daoBook->insert($book);
    } else if ($method === 'PUT') {
        parse_str(file_get_contents('php://input'), $_PUT);
        $book = new Book($_PUT['isbn'], $_PUT['name'], $_PUT['description'], $_PUT['rating'], $_PUT['image'], $_PUT['genreId'], $_PUT['author'], $_PUT['bookId']);
        $daoBook->update($book);
    } else if ($method === 'DELETE') {
        $daoBook->delete($_GET['bookId']);
    }
    
    echo $response;

?>