<?php

    include_once "../dao/DAOGenre.php";

    $daoGenre = new DAOGenre();

	if ($_SERVER['REQUEST_METHOD'] == 'GET') {
        $genres = $daoGenre->getAll();
        echo json_encode($genres);
    }
	
?>