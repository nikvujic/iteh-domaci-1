$(document).ready(() => {

    // data containers
    var booksPanel = document.getElementById('booksPanel');
    var genreContainer = document.getElementById('genreContainer');
    var newBookModal = document.getElementById('new-book-modal');

    // event refrences
    var refreshBtn = document.getElementById('refresh-btn');
    var sortBtn = document.getElementById('sort-btn');
    var searchBtn = document.getElementById('search-btn');
    var addNewBtn = document.getElementById('add-new-btn');
    var newBookSubmitBtn = document.getElementById('new-book-submit-btn');
    var editBtn = document.getElementById('edit-btn');
    var deleteBtn = document.getElementById('delete-btn');
    var editBookBtn = document.getElementById('edit-book-submit-btn');

    var loadedBooks = [];
    var sortDirection = false;

    let booksUrl = './php/server/books.php';
    let genresUrl = './php/server/genres.php';

    var selectedBookId = '';
    var selectedBook;
    var bookGenres = [];

    function setupListeners() {
        refreshBtn.addEventListener("click", () => {
            loadAllBooks();
        });
        sortBtn.addEventListener("click", () => {
            sortBooks();
        });
        searchBtn.addEventListener("click", () => {
            searchBooks();
        });
        addNewBtn.addEventListener("click", () => {
            openNewBookModal();
        });
        newBookSubmitBtn.addEventListener("click", () => {
            addNewBook();
        });
        editBtn.addEventListener("click", () => {
            editBook();
        });
        deleteBtn.addEventListener("click", () => {
            deleteBook();
        });
        editBookBtn.addEventListener("click", () => {
            updateBook();
        });

        document.body.addEventListener('click', function ( event ) {
            if (event.target.id == 'bookItem') {
                let bookId = event.target.getAttribute('data-bookId');

                if (bookId != null && bookId != undefined) {
                    selectBook(bookId);
                }
            };
        });
    }

    function loadAllBooks() {
        $.ajax({
            type: 'GET',
            url: booksUrl,
            success: function (data) {
                var books = JSON.parse(data);
                if (books.length > 0) {
                    console.log("Loaded books", data);
                    addBooksToDocument(books);
                    $bookItems = $('.book-item');
                } else {
                    alert('No books available!');
                }
            },
            error: function () {
                alert('Error occurred while loading all books!');
            }
        });
    }

    function addBooksToDocument(books) {
        booksPanel.innerHTML = '';
        loadedBooks = [];
        var booksHtml = '';
        for (let i = 0; i < books.length; i++) {
            booksHtml += createBookHtml(books[i]);
            loadedBooks.push(books[i].id);
        }
        booksPanel.innerHTML = booksHtml;
    }

    function createBookHtml(book) {
        let bookHtml = `
        <div class="row book-row">
            <div id="bookItem" class="col book-cover-col" data-bookId='${book.bookId}'>
                <img class="book-cover" src="./img/${book.image}" alt="">
                <p class="info-p">${book.name} <br> ${book.author} <br> ${book.rating}/5</p>
            </div>
        </div>`;

        return bookHtml;
    }

    function sortBooks() {
        if (loadedBooks.length == 0) {
            alert("No books loaded!");
            return;
        }

        $.ajax({
            type: 'GET',
            url: booksUrl + '?sort=true',
            success: function (data) {
                console.log(data);
                var books = JSON.parse(data);
                if (books.length > 0) {
                    console.log("Loaded books", data);
                    addBooksToDocument(books);
                    $bookItems = $('.book-item');

                    sortDirection = !sortDirection;
                } else {
                    alert('No books available!');
                }
            },
            error: function () {
                alert('Error occurred while loading all books!');
            }
        });
    }
    
    function searchBooks() {
        let searchWord = document.getElementById('book-name-input').value;

        if (searchWord.length == 0) {
            loadAllBooks();
            return;
        }

        $.ajax({
            type: 'GET',
            url: booksUrl + `?searchWord=${searchWord}`,
            success: function (data) {
                console.log(data);
                var books = JSON.parse(data);
                if (books.length > 0) {
                    console.log("Loaded books", data);
                    addBooksToDocument(books);
                    $bookItems = $('.book-item');
                } else {
                    alert('No books available!');
                }
            },
            error: function () {
                alert('Error occurred while loading all books!');
            }
        });
    }

    function openNewBookModal(edit = false) {
        newBookModal.style.display =  "block";

        if (!edit) {
            document.getElementById('new-book-submit-btn').style.display = "block";
            document.getElementById('edit-book-submit-btn').style.display = "none";
        } else {
            document.getElementById('new-book-submit-btn').style.display = "none";
            document.getElementById('edit-book-submit-btn').style.display = "block";
        }

        window.onclick = function(event) {
            if (event.target == newBookModal) {
                closeNewBookModal();
                clearModal();
            }
        }
    }

    function loadGenres() {
        $.ajax({
            type: 'GET',
            url: genresUrl,
            success: function (data) {
                var genres = JSON.parse(data);
                addGenresToDocument(genres);
                console.log(data);
            },
            error: function (data) {
                alert('Error occurred while loading genres!');
                console.log(data);
            }
        });
    }

    function closeNewBookModal() {
        newBookModal.style.display = "none";
    }

    function addGenresToDocument(genres) {
        bookGenres = genres;
        genreContainer.innerHTML = '';
        loadedGenres = [];
        var genresHtml = '';
        for (let i = 0; i < genres.length; i++) {
            genresHtml += createGenreHtml(genres[i]);
        }
        genreContainer.innerHTML = genresHtml;
    }
    
    function createGenreHtml(genre) {
        let genreHtml = `<option value="${genre.genreId}">${genre.name}</option>`;
        return genreHtml;
    }

    function addNewBook() {
        newBook = getBookFromForm();

        if (newBook.name == null || newBook.name == undefined || newBook.name.length == 0) {
            alert("Name field for new book can't be empty.");
            return;
        }

        if (newBook.author == null || newBook.author == undefined || newBook.author.length == 0) {
            alert("Author field for new book can't be emptry.");
            return;
        }

        if (newBook.isbn == null || newBook.isbn == undefined || newBook.isbn.length == 0) {
            alert("ISBN field for new book can't be empty.");
            return;
        }

        if (newBook.rating == null || newBook.rating == undefined) {
            alert("New book must have rating selected.");
            return;
        }

        $.ajax({
            type: 'POST',
            url: booksUrl,
            data: newBook,
            success: function (data) {
                console.log(data);
                loadAllBooks();
                alert('New book has been successfully created!');
                clearModal();
                closeNewBookModal();
            },
            error: function () {
                alert('Error occurred while creating a new book!');
            }
        });
    }

    function getBookFromForm() {
        let isbn = document.getElementById('new-book-isbn').value;
        let name = document.getElementById('new-book-name').value;
        let description = document.getElementById('description-textarea').value;
        let rating = getNewBookRating();
        let author = document.getElementById('new-book-author').value;
        let image = 'default.png';

        let select = document.getElementById('genreContainer');

        let genreId = select.options[select.selectedIndex].value;

        return {
            isbn: isbn,
            name: name,
            description: description,
            rating: rating,
            author: author,
            image: image,
            genreId: genreId
        };
    }

    function getNewBookRating() {
        let rating;

        try {
            rating = document.querySelector('input[name="new-rating"]:checked').value;
        } catch (e) {
            rating = null
        }

        return rating;
    }

    function selectBook(bookId) {
        this.selectedBookId = "";

        clearSelectedBookData();

        $.ajax({
            type: 'GET',
            url: booksUrl + '?bookId=' + bookId,
            success: function (data) {
                var book = JSON.parse(data);
                selectedBookId = bookId;
                fillSelectedBookData(book);
            },
            error: function () {
                alert('Error occurred while loading book with bookId = ' + bookId + '!');
            }
        });
    }

    function fillSelectedBookData(book) {
        genre = bookGenres.find(e => e.genreId == book.genreId);

        document.getElementById("selected-book-img").src = `./img/${book.image}`;
        document.getElementById('selected-book-rating').innerHTML = `${book.rating}/5`;
        document.getElementById('selected-book-isbn').innerHTML = book.isbn;
        document.getElementById('selected-book-name').innerHTML = book.name;
        document.getElementById('selected-book-genre').innerHTML = genre.name;
        document.getElementById('selected-book-author').innerHTML = book.author;
        document.getElementById('selected-book-description').innerHTML = book.description;

        selectedBook = book;
    }

    function clearSelectedBookData() {
        selectedBookId = '';

        document.getElementById("selected-book-img").src = `./img/default.png`;
        document.getElementById('selected-book-rating').innerHTML = "Rating";
        document.getElementById('selected-book-isbn').innerHTML = "ISBN";
        document.getElementById('selected-book-name').innerHTML = "Book name";
        document.getElementById('selected-book-genre').innerHTML = "Genre";
        document.getElementById('selected-book-author').innerHTML = "Author";
        document.getElementById('selected-book-description').innerHTML = "Description";

        selectedBook = undefined;
    }

    function editBook() {
        if (selectedBookId == "") {
            alert("Book not selected!");
            return;
        }

        clearModal();

        if (selectedBook == undefined || selectedBook == null) {
            alert("Book not selected!");
            return;
        }

        openNewBookModal(true);

        document.getElementById('new-book-isbn').value = selectedBook.isbn;
        document.getElementById('new-book-name').value = selectedBook.name;
        document.getElementById('description-textarea').value = selectedBook.description;
        document.getElementById('new-book-author').value = selectedBook.author;

        document.getElementById(`rating${selectedBook.rating}`).checked = true;

        genre =  bookGenres.find(e => e.genreId == selectedBook.genreId);

        document.getElementById('genreContainer').value = genre.genreId;
    }

    function deleteBook() {
        if (selectedBookId == "") {
            alert("Book not selected!");
            return;
        }

        $.ajax({
            type: 'DELETE',
            url: booksUrl + '?bookId=' + selectedBookId,
            success: function () {
                alert('Book has been successfully deleted!');
                loadAllBooks();
                clearSelectedBookData();
            },
            error: function () {
                alert('Error occurred while deleting a book!');
            }
        });
    }

    function clearModal() {
        document.getElementById('new-book-isbn').value = "";
        document.getElementById('new-book-name').value = "";
        document.getElementById('description-textarea').value = "";
        document.getElementById('new-book-author').value = "";

        var radioInputs = document.querySelectorAll('.rating');

        radioInputs.forEach((el) => {
            el.checked = false;
        });
    }

    function updateBook() {
        let book = getBookFromForm();

        book.bookId = selectedBookId;

        updateBookReq(book);
    }

    function updateBookReq(book) {
        $.ajax({
            type: 'PUT',
            url: booksUrl,
            data: book,
            success: function () {
                alert('Book has been successfully updated...');
                loadAllBooks();
                clearModal();
                closeNewBookModal();
                clearSelectedBookData();
            },
            error: function () {
                alert('Error occurred while updating a book...');
            }
        });
    }

    setupListeners();
    loadGenres();
});