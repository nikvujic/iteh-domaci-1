Potrebno je napraviti aplikaciju proizvoljnog sadržaja, koristeći PHP, MySQLi AJAX tehnologije. Zadatak radi svaki student pojedinačno.

Zahtevi:

1. Neophodno je da postoji baza (sa bar dve tabele koje su međusobno povezane) nad kojom moraju biti implementirane osnovne operacije (INSERT, UPDATE, DELETE, SELECT). 
2. Potrebno je da postoje bar dve primene AJAX-a. 
3. Za najvišu ocenu neophodno je obezbediti bar dve dodatne funkcionalnosti (pretraga, sortiranje, upravljanje greškama i sl.) i koristiti JavaScript ili jQuery. 
4. Domaći raditi bez korišćenja frejmvorka. 

Poželjno je da sajt ima jedinstven dizajn uz korišćenje CSS-a i frontend frejmvorka (Bootstrap, Foundation, itd.). 

Predaje se arhiva sa aplikacijom i bazom (bazu je potrebno exportovati) i tekstualni fajl u kojem se nalazi kratak opis aplikacije i svih njenih fajlova i funkcionalnosti.

Prilikom rada u PHP–u voditi računa o sledećem:

* Koristiti neke od PHP funkcija, kao što su require, include, date,
* Obavezno koristiti superglobalne varijable,
* Neophodno je koristiti OOP koncepte u PHPu,
* Kreirati dovoljno bogate forme.
* Kod je potrebno postaviti na GitHub i vršiti redovno verzionisanje koda sa smislenim porukama

Termini za odbranu tokom semestra su u terminu konsultacija i odbrane se vrše na Moodle platformi. Potrebno je dogovoriti termin odbrane sa nekim od nastavnika i saradnika (Tamara Naumović tamara@elab.rs , Petar Lukovac petarlukovac@elab.rs, Aleksa Miletić aleksamiletic@elab.rs). 

Tokom ispitnih rokova, zadaci će se braniti u posebno utvrđenim terminima, uz obaveznu prijavu preko sajta.


Za odbranu je potrebno pripremiti aplikaciju sa bazom i tekstualni fajl u kojem se nalazi opis aplikacije i svih njenih fajlova i funkcionalnosti. Posebno će se ceniti kreativnost i samostalnost u radu. Kodovi sa vežbi ili sa veba mogu poslužiti kao uzor, ali se ne mogu koristiti u nepromenjenom obliku.

Pre odbrane, neophodno je okačiti rešenje zadatka (kompresovani fajl koji sadrži sve potrebne fajlove aplikacije i izvezenu bazu podataka sa nazivom u sledećem formatu Petar_Peric_20180001_Domaci1) na Moodle.